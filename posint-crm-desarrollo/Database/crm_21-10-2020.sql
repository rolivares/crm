ALTER TABLE `tblgoods_receipt` ADD `order` VARCHAR(15) NOT NULL AFTER `approval`;
ALTER TABLE `tblgoods_delivery` ADD `bill` VARCHAR(30) NOT NULL AFTER `addedfrom`;
ALTER TABLE `tblgoods_delivery` ADD `status` VARCHAR(13) NOT NULL AFTER `bill`;