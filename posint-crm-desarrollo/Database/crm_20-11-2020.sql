CREATE TABLE `tblstatus` (
  `id` int(3) NOT NULL,
  `id_status` int(3) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



INSERT INTO `tblstatus` (`id`, `id_status`, `name`) VALUES
(1, 1, 'POR PAGAR'),
(2, 2, 'PAGADA'),
(3, 3, 'PAGADA PARCIALMENTE');


ALTER TABLE `tblstatus`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tblstatus`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;